(in-package #:nine-p)


;; size[4] Tflush tag[2] oldtag[2]
;; size[4] Rflush tag[2]

(defclass 9p-tflush (9p-message)
  ((old-tag :initarg :old-tag :accessor old-tag-of))
  (:documentation "Abort request message."))

(defmethod message-size ((m 9p-tflush))
  (+ binary:U16 ; old-tag
     (call-next-method)))

(defun make-9p-tflush (old-tag &key header (tag notag))
  (let ((message
	 (make-instance '9p-tflush
			:old-tag old-tag
			:header
			(if header header
			    (make-instance '9p-message-header
					   :tag tag
					   :type TAUTH)))))
    (setf (message-length-of (message-header-of message))
	  (message-size message))
    message))

(defun read-9p-tflush (header stream)
  (make-9p-tflush (binary:read-u16 stream) :header header))

(defun write-9p-tflush (m stream)
  (binary:write-u16 stream (old-tag-of m)))

(defclass 9p-rflush (9p-message)
  () ; no additional slots needed
  (:documentation "Abort response message."))

(defmethod message-size ((m 9p-rflush))
  (call-next-method))

(defun make-9p-rflush (&key header (tag notag))
  (let ((message
	 (make-instance '9p-tflush
			:header
			(if header header
			    (make-instance '9p-message-header
					   :tag tag
					   :type TAUTH)))))
    (setf (message-length-of (message-header-of message))
	  (message-size message))
    message))

(defun read-9p-rflush (header stream)
  (declare (ignore stream))
  (make-9p-rflush :header header))

(defun write-9p-rflush (m stream)
  (declare (ignore m stream)))
