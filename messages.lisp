(in-package #:nine-p)

(defgeneric message-size (m)
  (:documentation "Compute the size of a message."))
(defmethod message-size (m)
  (declare (ignore m))
  (error "Invalid message."))

(defclass 9p-message-header ()
  ((type   :initarg :type   :accessor message-type-of)
   (tag    :initarg :tag    :accessor message-tag-of)
   (length :initarg :length :accessor message-length-of))
  (:documentation "A Plan 9 File Protocol message type, tag, and the
  number of bytes remaining in the body."))

(defmethod message-size ((header 9p-message-header))
  (+ binary:U8  ; type field
     binary:U16 ; tag field
))

(defun read-header (stream)
    (let* ((message-length (binary:read-u32 stream))
	 (message-body-length
	  (- message-length binary:u32 binary:U8 binary:U16))
	 (header (make-instance
		  '9p-message-header
		  :type (binary:read-u8 stream)
		  :tag  (binary:read-u16 stream)
		  :length message-body-length)))
    (describe header)
    header))

(defun write-header (header stream)
  (binary:write-u8  stream (message-type-of header))
  (binary:write-u16 stream (message-tag-of header)))

(defclass 9p-message ()
  ((header :initarg :header :accessor message-header-of))
  (:documentation "Superclass for 9P protocol messages."))

(defmethod message-size ((message 9p-message))
  (+ binary:U32 ; message size
     (message-size (message-header-of message))))
