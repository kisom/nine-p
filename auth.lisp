(in-package #:nine-p)


;; size[4] Tauth tag[2] afid[4] uname[s] aname[s]
;; size[4] Rauth tag[2] aqid[13]


(defclass 9p-tauth (9p-message)
  ((afid :initarg :afid :accessor afid-of)
   (uname :initarg :uname :accessor :uname-of)
   (aname :initarg :aname :accessor :aname-of))
  (:documentation "Authentication request message."))

(defmethod message-size ((m 9p-tauth)) 
  (+ binary:U32 ; afid
     binary:U16 ; uname length
     (trivial-utf-8:utf-8-byte-length (uname-of m))
     binary:U16 ; aname length
     (trivial-utf-8:utf-8-byte-length (aname-of m))
     (call-next-method)))

(defun make-9p-tauth (afid uname aname &key header (tag notag))
  (let ((message
	 (make-instance '9p-tauth
			:afid afid
			:uname (trivial-utf-8:string-to-utf-8-bytes uname)
			:aname (trivial-utf-8:string-to-utf-8-bytes aname)
			:header
			(if header header
			    (make-instance '9p-message-header
					   :tag tag
					   :type TAUTH)))))
    (setf (message-length-of (message-header-of message))
	  (message-size message))
    message))

(defun read-9p-tauth (header stream)
  (let ((afid  (binary:read-u32 stream))
	(uname (read-utf-string stream))
	(aname (read-utf-string stream)))
    (make-9p-tauth afid uname aname :header header)))

(defun write-9p-tauth (m stream)
  (binary:write-u32 stream (fid-of m))
  (write-utf-string stream (uname-of m))
  (write-utf-string stream (aname-of m)))

(defclass 9p-rauth (9p-message)
  ((aqid :initarg :aqid :accessor aqid-of))
  (:documentation "Authentication response message."))

(defmethod message-size ((m 9p-rauth)) 
  (+ 13 ; aqid is 13 bytes
     (call-next-method)))

(defun make-9p-rauth (aqid &key (tag notag) header)
  (let ((message
	 (make-instance '9p-rauth
			:aqid aqid
			:header
			(if header header
			    (make-instance '9p-message-header
					   :tag tag
					   :type RAUTH)))))
    (setf (message-length-of (message-header-of message))
	  (message-size message))
    message))

(defun read-9p-rauth (header stream)
  (let ((aqid (binary:octets stream 13)))
    (make-9p-rauth :aqid aqid :header header)))

(defun write-9p-rauth (m stream)
  (write-sequence (aqid-of m) stream))
