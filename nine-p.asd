;;;; nine-p.asd

(asdf:defsystem #:nine-p
  :description "Common Lisp encoding and decoding Plan 9 File Protocol messages."
  :author "Kyle Isom <kyle@metacircular.net>"
  :license "MIT License"
  :depends-on (#:binary
               #:usocket
	       #:trivial-utf-8
	       #:flexi-streams)
  :serial t
  :components ((:file "package")
	       (:file "constants")
	       (:file "util")
	       (:file "messages")
	       (:file "version")
	       (:file "auth")
	       (:file "error")
	       (:file "flush")
               (:file "nine-p")))

