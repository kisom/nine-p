(in-package #:nine-p)


;; size[4] Rerror tag[2] ename[s]


(defclass 9p-rerror (9p-message)
  ((ename :initarg ename :accessor ename-of))
  (:documentation "Error response message."))

(defmethod message-size ((m 9p-rerror))
  (+ binary:U16 ; ename length
     (trivial-utf-8:utf-8-byte-length (ename-of m))
     (call-next-method)))

(defun make-9p-rerror (ename &key header (tag notag))
  (let ((message
	 (make-instance '9p-rerror
			:ename (trivial-utf-8:string-to-utf-8-bytes ename)
			:header
			(if header header
			    (make-instance '9p-message-header
					   :tag tag
					   :type RERROR)))))
    (setf (message-length-of (message-header-of message))
	  (message-size message))
    message))

(defun read-9p-rerror (header stream)
  (make-9p-rerror (read-utf-string stream) :header header))

(defun write-9p-rerror (m stream)
  (write-utf-string stream (ename-of m)))
