(in-package #:nine-p)

(defun fversion ()
  "Returns the default protocol version string."
  "9P2000")


;; size[4] Tversion tag[2] msize[4] version[s]
;; size[4] Rversion tag[2] msize[4] version[s]


(defclass 9p-version (9p-message)
  ((max-size :initarg :max-size :accessor max-size-of)
   (version  :initarg :version  :accessor version-of))
  (:documentation "9P protocol version message."))

(defun valid-version-string (version-string)
  (let ((prefix-index (search "9P" version-string)))
    (or (and (numberp prefix-index) (zerop prefix-index))
	(string-equal version-string "unknown"))))

(defmethod message-size ((version 9p-version))
  (+ binary:U32 ; max-size
     binary:U16 ; version length
     (trivial-utf-8:utf-8-byte-length (version-of version))
     (call-next-method)))

(defun make-9p-version (msize &key (tag notag) (version (fversion)) reply header)
  (when (valid-version-string version)
    (let ((message
	   (make-instance '9p-version
			  :version version
			  :max-size msize
			  :header
			  (if header header
			      (make-instance '9p-message-header
					     :tag tag
					     :type (if reply
						       RVERSION
						       TVERSION))))))
      (setf (message-length-of (message-header-of message))
	    (message-size message))
      message)))

(defun read-9p-version (header stream)
  (let ((msize   (binary:read-u32 stream))
	(version (read-utf-string stream)))
    (make-9p-version msize :version version
		     :header header
		     :reply (eql (message-type-of header) RVERSION))))

(defun write-9p-version (version stream)
  (binary:write-u32 stream (max-size-of version))
  (write-utf-string stream (version-of version)))
