(in-package #:nine-p)

;;;; Constant definitions from fcall.h.

;; Dummy tag
(defconstant NOTAG 65535)      ; defined as a U16 ~0

;; Dummy FID
(defconstant NOFID 4294967295) ; defined as a U32 ~0

;; Dummy UID
(defconstant NOUID -1)         ; no size specifier


;;; Tags

(defmacro deftag (name value)
  `(progn
     (defconstant ,(intern (format nil "T~A" name)) ,value)
     (defconstant ,(intern (format nil "R~A" name)) ,(+ value 1))))

(deftag version 100)
(deftag auth    102)
(deftag attach  104)
(deftag error   106)
(deftag flush   108)
(deftag walk    110)
(deftag open    112)
(deftag create  114)
(deftag read    116)
(deftag write   118)
(deftag clunk   120)
(deftag remove  122)
(deftag stat    124)
(deftag wstat   126)
(deftag openfd   98)
(defconstant TMAX 128)
