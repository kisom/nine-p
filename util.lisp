(in-package #:nine-p)

(defun read-utf-string (stream)
  (let ((size (binary:read-u16 stream)))
    (trivial-utf-8:read-utf-8-string 
     stream
     :byte-length size)))

(defun write-utf-string (stream s)
  (let ((size (trivial-utf-8:utf-8-byte-length s)))
    (binary:write-u16 stream size)
    (write-sequence
     (trivial-utf-8:string-to-utf-8-bytes s)
     stream)))
