;;;; nine-p.lisp

(in-package #:nine-p)

;;; "nine-p" goes here. Hacks and glory await!

(defmacro rmatch (&rest body)
  `(cond
     ,@(loop for condform in body
	  collecting `((eql mtype ,(caar condform))
		       (,(cadar condform) header stream)))
     (t (error "Invalid message."))))

(defun read-message (stream)
  "Read a 9P message from the stream."
  (let* ((header (read-header stream))
	 (mtype (message-type-of header)))
    (rmatch 
      ((TVERSION read-9p-version))
      ((RVERSION read-9p-version))
      ((TAUTH    read-9p-tauth))
      ((RAUTH    read-9p-rauth))
      ((RERROR   read-9p-rerror))
      ((TFLUSH   read-9p-tflush))
      ((RFLUSH   read-9p-rflush)))))

(defmacro wmatch (&rest body)
  `(cond
     ,@(loop for condform in body
	  collecting `((eql mtype ,(caar condform))
		       (,(cadar condform) message stream)))
     (t (error "Invalid message."))))

(defun write-message (message stream)
  (let ((size (message-size message)))
    (binary:write-u32 stream size)
    (write-header (message-header-of message) stream)
    (let ((mtype (message-type-of (message-header-of message))))
      (wmatch 
       ((TVERSION write-9p-version))
       ((RVERSION write-9p-version))
       ((TAUTH    write-9p-tauth))
       ((RAUTH    write-9p-rauth))
       ((RERROR   write-9p-rerror))
       ((TFLUSH   write-9p-tflush))
       ((RFLUSH   write-9p-rflush))))))
